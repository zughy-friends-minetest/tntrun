local S = minetest.get_translator("tntrun")

-- Utility functions

-- Given two positions of an area, returns them so
-- that the first position has the minimum, and the
-- second position has the maximum value.
function tntrun.sort_positions(pos1, pos2)
	local ppos1 = table.copy(pos1)
	local ppos2 = table.copy(pos2)
	local coords = { "x", "y", "z" }
	for c=1, #coords do
		local coord = coords[c]
		if pos1[coord] > pos2[coord] then
			ppos1[coord] = pos2[coord]
			ppos2[coord] = pos1[coord]
		end
	end
	return ppos1, ppos2
end

-- Checks the TNT areas of the given arena for syntactical validity.
-- Returns true if everything is OK, false otherwise.
-- In case of failure, 2nd return value is localized error message
-- that can be shown to an user
function tntrun.check_tnt_areas(arena)
	-- Perform a couple of syntax checks for the TNT areas
	if #arena.tnt_area_pos_1 ~= #arena.tnt_area_pos_2 then
		return false, S("Invalid arena properties! Number of positions in `tnt_area_pos_1` and `tnt_area_pos_2` is not equal!")
	end
	local props = {"tnt_area_pos_1", "tnt_area_pos_2"}
	for p=1, #props do
		local propname = props[p]
		local prop = arena[propname]
		for i=1, #prop do
			local val = prop[i]
			if type(val) ~= "table" then
				return false, S("Invalid arena property `@1`: Position is not a table!", propname)
			elseif type(val.x) ~= "number" or type(val.y) ~= "number" or type(val.z) ~= "number" then
				return false, S("Invalid arena property `@1`: Coordinate x, y or z missing or not a number!", propname)
			end
		end
	end
	return true
end

-- Checks whether all TNT areas are within the arena region, if present.
-- Returns true if all areas are in the arena region OR there is no arena region.
-- Returns false otherwise. In case of failure, the 2nd return value
-- is an error message that can be shown to the user.
-- Note: This function assumes the TNT area properties are syntactically valid.
function tntrun.are_tnt_areas_in_arena_region(arena)
	if not arena.pos1 or not arena.pos2 then
		return true
	end
	local arpos1, arpos2 = tntrun.sort_positions(arena.pos1, arena.pos2)
	for a=1, #arena.tnt_area_pos_1 do
		local tpos1 = arena.tnt_area_pos_1[a]
		local tpos2 = arena.tnt_area_pos_2[a]
		tpos1, tpos2 = tntrun.sort_positions(tpos1, tpos2)
		-- Compare the TNT area bounds with the arena region
		if tpos1.x < arpos1.x or tpos1.y < arpos1.y or tpos1.z < arpos1.z or
				tpos2.x > arpos2.x or tpos2.y > arpos2.y or tpos2.z > arpos2.z then
			return false, S("TNT area #@1 is not within the arena region.", a)
		end
	end
	return true
end

-- Checks the arena properties of the given arena for
-- validity.
-- Returns true if everything is OK, false otherwise.
-- In case of failure, 2nd return value is localized error message
-- that can be shown to an user
function tntrun.check_arena_properties(arena)
	return tntrun.check_tnt_areas(arena)
end

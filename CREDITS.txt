Thanks to KevinMacLeod for his wonderful music Crusade (Creative Commons Attribution 4.0 International License).
Thanks to JasonShaw at audionautix.com for his wonderful music AllGoodInTheWood -Creative Commons Attribution 4.0 International License. music by audionautix.com
Thanks to ShadowNinja for the smoke puff texture `tntrun_smoke_puff.png` (Creative Commons Attribution-ShareAlike 3.0 Unported License)
Thanks to Giov4 for the mod icon (Creative Commons Attribution 4.0 International License).
Thanks to Giov4 for the round start sound `tntrun_round_start.ogg` (Creative Commons Attribution-ShareAlike 4.0 International License).
Thanks to ManDaKi for the Burmese Xylophone sample <https://freesound.org/people/ManDaKi/sounds/696317/>, used to create `tntrun_countdown.ogg` (CC0)
Thanks to Pablobd for the fuse sound <https://freesound.org/people/Pablobd/sounds/490518/>, used to create `tntrun_fuse.ogg` (CC0)
Thanks to Zughy for the torch, TNT textures and editor textures (Creative Commons Attribution-ShareAlike 4.0 International License)
Thanks to various anonymous authors for CC0 sounds which were used to create:
	- tntrun_win.ogg
	- tntrun_elim.ogg
	- tntrun_lose.ogg
	- tntrun_swish.ogg
Thanks to Minetest Game developers for the TNT textures (Creative Commons Attribution Share-Alike 3.0 International) (see textures/LICENSE.txt for details)

## Creative Commons license links:

* CC0: https://creativecommons.org/publicdomain/zero/1.0/
* Attribution 4.0 Int'l: http://creativecommons.org/licenses/by/4.0/
* Attribution Share-Alike 3.0 Unported: http://creativecommons.org/licenses/by-sa/3.0/
* Attribution Share-Alike 4.0 Int'l: http://creativecommons.org/licenses/by-sa/4.0/

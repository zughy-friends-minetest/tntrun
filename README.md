# Tntrun documentation

## Table of Contents
* [1. Gameplay](#1-gameplay)
* [2. Create an arena](#2-create-an-arena)
* [3. Arena properties (advanced users)](#3-arena-properties-advanced-users)

## 1. Gameplay

In Tntrun, each player spawns on a fragile platform of TNT and they all get a torch.
TNT will fall down when punched with the torch (immediately) or when walked on (after a brief delay).
When a player has fallen all the way down, they are eliminated.

The goal is to be the last player standing.

## 2. Create an arena

1. Type in chat:
```
/arenas create tntrun <your arena name> <min. players for arena> <max. players for arena>
```

2. Type in chat
```
/arenas edit <your arena name>
```

3. Go to spawners and set up where you can spawn
4. Set up an arena sign
5. (Optional) Go to BGM and choose your background music. It must be put in the `world/arena_lib/BGM` directory and in the `.ogg` file format
6. (Optional, but recommended) Go to the settings and set the arena region in which the game will take place
7. Go to "TNT areas and elimination height"
8. Use the items to set up the TNT areas and elimination height (see below)
9. Save settings
10. Now you can play

### The Tntrun editor items

#### Quick start

Use the tool "Set TNT area position" with left-/rightclick
(or more generally, your punch/place keys)
to set the two corners of the area where the TNT will spawn.

Make sure the area is completely within the arena region.

Then fly to the height where players will die and use
"Set elimination height".

And that's basically it! If you want multiple TNT layers, read on.

#### Details

Here's how it works: Tntrun will automatically place TNT on the start of
each game so you need to specify their positions.

You can specify one or more 3-dimensional TNT areas which are enclosed
between two corners (position 1, position 2).

Specifically, when the game loads or ends, Tntrun will replace all
air nodes within all TNT areas with TNT. Other nodes will be left
alone.

If there are any TNT areas, one of them will be 'selected'. This is the area
which will be affected by editing and deletion. The "Select TNT area"
item will show the number of the selected area and the total number of TNT areas.
If there are no areas, it will show "N/A".

Explanation of the editor items:

* Add TNT area: Adds a new TNT area at your position and selects it
* Remove TNT area: Removes the currently selected TNT area
* Remove all TNT areas: Self-explanatory
* Select TNT area: Use left-/rightclick to select the next or previous TNT area
* Set TNT area position: Will set your current position as position 1 or position 2 of the current TNT area (left-/rightclick)
* Regenerate TNT: Will remove all TNT in the arena region, then place TNT node at all TNT areas
* Set elimination height: Set your current Y coordinate as the elimination height below which players will die

Note: You don't have to use Regenerate TNT to make your arena work because Tntrun will
automatically regenerate on every load. You can use this item for preview and cleanup.

The HUD will show you the positions of the corners (pos1, pos2) of all TNT areas.
The numbers 1 and 2 on the icons stand for position 1 and 2.
The selected area will have opaque icons while the other areas will be semitransparent.

The elimination height is also represented by an icon at the center of the arena region
on the XZ plane.
This icon is not shown when there is no arena region (but elimination will still work).

## 3. Arena properties (advanced users)

The section "TNT areas and elimination height" already gives
you everything you need so editing the arena properties
by hand is not necessary.
You can still configure the arena that way if you choose to.

### `tnt_area_pos_1` and `tnt_area_pos_2`

The arena properties `tnt_area_pos_1` and `tnt_area_pos_2` specify
the TNT areas.

For a single TNT area, you must write the arena settings like this:

```
tnt_area_pos_1 = {{ x=0, y=0, z=0 }}
tnt_area_pos_2 = {{ x=10, y=0, z=10 }}
```

Where the numbers after x, y and z are the coordinates of the two corners
of the TNT layer.

For multiple TNT areas, you just create a comma-separated list of positions.
It looks like this:

```
tnt_area_pos_1 = {{ x=0, y=0, z=0 },   { x=0, y=6, z=0 },   { x=0, y=12, z=0 }}
tnt_area_pos_2 = {{ x=10, y=0, z=10 }, { x=10, y=6, z=10 }, { x=10, y=12, z=10 }}
```

This will create 3 square layers on Y=0, Y=6 and Y=12, between the corners X=0,Z=0 and X=10,Z=10.

Be careful to use the correct syntax or else Tntrun will refuse to work.
Also, the number of positions in both `tnt_area_pos_*` settings *must* be equal.

### `eliminate_height`

This is a number. It is the Y height below which players will die and get eliminated.
